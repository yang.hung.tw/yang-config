"--------------
" key mapping
"--------------
" disable arrow keys
noremap <UP> <NOP>
noremap <DOWN> <NOP>
noremap <LEFT> <NOP>
noremap <RIGHT> <NOP>
inoremap <UP> <NOP>
inoremap <DOWN> <NOP>
inoremap <LEFT> <NOP>
inoremap <RIGHT> <NOP>

nmap <leader>ne :NERDTree<CR>

" previous, next, and delete buffer
map ;n :bn<CR>
map ;p :bp<CR>
map ;d :bd<CR>
