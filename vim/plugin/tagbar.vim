let g:tagbar_width = 25      " window size
let g:tagbar_autofocus = 1   " focus on tabbar window
let g:tagbar_sort = 0        " disable sorting by default
nmap <F8> :TagbarToggle<CR>
