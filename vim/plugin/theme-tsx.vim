" dark red
hi tsxTagName ctermfg=203 guifg=#E06C75
hi tsxComponentName ctermfg=203 guifg=#E06C75
hi tsxCloseComponentName ctermfg=203 guifg=#E06C75
" orange
hi tsxCloseString ctermfg=208 guifg=#F99575
hi tsxCloseTag ctermfg=208 guifg=#F99575
hi tsxCloseTagName ctermfg=208 guifg=#F99575
hi tsxAttributeBraces ctermfg=208 guifg=#F99575
hi tsxEqual ctermfg=208 guifg=#F99575
" yellow
hi tsxAttrib ctermfg=214 guifg=#F8BD7F cterm=italic"
" light-grey
hi tsxTypeBraces ctermfg=144 guifg=#999999
" dark-grey
hi tsxTypes ctermfg=102 guifg=#666666"
hi ReactState ctermfg=175 guifg=#C176A7
hi ReactProps ctermfg=166 guifg=#D19A66
hi ApolloGraphQL ctermfg=132 guifg=#CB886B
hi Events ctermfg=71 guifg=#56B6C2
hi ReduxKeywords ctermfg=137 guifg=#C678DD
hi ReduxHooksKeywords ctermfg=175 guifg=#C176A7
hi WebBrowser ctermfg=71 guifg=#56B6C2
hi ReactLifeCycleMethods ctermfg=172 guifg=#D19A66

